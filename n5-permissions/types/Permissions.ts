interface Permissions {
  id: string;
  employeeName: string;
  employeeLastName: string;
  permissionType: string;
  permissionDate: string;
}
