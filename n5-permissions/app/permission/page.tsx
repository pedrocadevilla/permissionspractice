"use client";
import { useEffect, useState } from "react";
import LoadingSpinner from "../components/loading-spinner";
import Typography from "@mui/material/Typography";
import PermissionTable from "../components/permission-table";
import CreateForm from "../components/create-form";
import Grid from "@mui/material/Grid";
import axios from "axios";

export default function Permission() {
  const [showSpinner, setSpinner] = useState<boolean>(true);
  const [permissions, setPermissions] = useState<Permissions[]>([]);
  const [permissionsType, setPermissionsType] = useState<PermissionsTypes[]>(
    []
  );

  useEffect(() => {
    axios
      .get("http://localhost:8080/permissionType")
      .then((res) => {
        setPermissionsType(res.data.value);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setSpinner(false);
      });
  }, []);

  return (
    <Grid container direction={"row"} spacing={3}>
      {showSpinner && <LoadingSpinner />}
      <Grid item xs={12}>
        <Typography variant="h3" align="center" gutterBottom>
          Permissions Assigned
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <CreateForm
          setSpinner={setSpinner}
          setPermissions={setPermissions}
          permissionType={permissionsType}
        ></CreateForm>
      </Grid>
      <Grid item xs={12}>
        <PermissionTable
          setSpinner={setSpinner}
          setPermissions={setPermissions}
          permissions={permissions}
          permissionTypes={permissionsType}
        ></PermissionTable>
      </Grid>
    </Grid>
  );
}
