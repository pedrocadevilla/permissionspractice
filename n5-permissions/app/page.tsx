import Grid from "@mui/material/Grid/Grid";
import PresentationCard from "./components/presentation-card";
import FunctionalityCard from "./components/functionality-card";

export default function Home() {
  return (
    <Grid
      container
      spacing={5}
      display="flex"
      direction="row"
      justifyContent="space-between"
      alignItems="stretch"
    >
      <Grid item xs={12} sm={6} key="1">
        <PresentationCard></PresentationCard>
      </Grid>
      <Grid item xs={12} sm={6} key="2">
        <FunctionalityCard></FunctionalityCard>
      </Grid>
    </Grid>
  );
}
