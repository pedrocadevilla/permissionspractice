import * as React from "react";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import { useEffect, useState } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";

export default function CreateForm(props: {
  setSpinner: (arg0: boolean) => void;
  setPermissions: (arg0: Permissions[]) => void;
  permissionType: PermissionsTypes[];
}) {
  const [formData, setFormData] = useState({
    EmployeeName: "",
    EmployeeLastName: "",
    PermissionTypeId: "",
  });

  const handleInput = (e: any) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;

    setFormData((prevState) => ({
      ...prevState,
      [fieldName]: fieldValue,
    }));
  };

  const createPermission = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent> | undefined
  ) => {
    event?.preventDefault();
    props.setSpinner(true);
    axios
      .post("http://localhost:8080/permission", formData)
      .then(() => {
        getPermissions();
        setFormData({
          EmployeeName: "",
          EmployeeLastName: "",
          PermissionTypeId: "",
        });
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        props.setSpinner(false);
      });
  };

  const getPermissions = () => {
    axios
      .get("http://localhost:8080/permissions")
      .then((res) => {
        props.setPermissions(res.data.value);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        props.setSpinner(false);
      });
  };

  return (
    <Grid
      component="form"
      container
      spacing={1}
      columns={16}
      noValidate
      autoComplete="off"
      justifyContent={"space-between"}
      alignItems={"center"}
    >
      <Grid item xs={4}>
        <TextField
          id="name"
          name="EmployeeName"
          label="First Name"
          variant="outlined"
          onChange={handleInput}
          value={formData.EmployeeName}
        />
      </Grid>
      <Grid item xs={4}>
        <TextField
          id="last"
          name="EmployeeLastName"
          label="Last Name"
          variant="outlined"
          onChange={handleInput}
          value={formData.EmployeeLastName}
        />
      </Grid>
      <Grid item xs={4}>
        <TextField
          fullWidth
          id="permission"
          select
          label="Permission"
          name="PermissionTypeId"
          defaultValue=""
          onChange={handleInput}
          value={formData.PermissionTypeId}
        >
          {props.permissionType.map((p) => (
            <MenuItem key={p.id} value={p.id}>
              {p.description}
            </MenuItem>
          ))}
        </TextField>
      </Grid>
      <Grid item xs={2}>
        <Button
          variant="outlined"
          disabled={
            formData.EmployeeName === "" ||
            formData.EmployeeLastName === "" ||
            formData.PermissionTypeId === ""
          }
          onClick={(e) => createPermission(e)}
        >
          Create
        </Button>
      </Grid>
    </Grid>
  );
}
