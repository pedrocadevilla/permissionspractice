import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

export default function PresentationCard() {
  return (
    <Card sx={{ minHeight: "100%" }}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          Pedro Cadevilla Teach Lead
        </Typography>
        <Typography variant="body2" color="text.secondary" align="justify">
          I am a bilingual with degree in Computer Science. I have experience in
          search engine development, mobile and web app development with
          third-party integrations, automated chatbot development, and app
          refactoring and security enhancements. I have also worked as a
          technical lead, upgrading and maintaining a platform, and have
          experience in backend and frontend development, database management,
          and cloud services.
        </Typography>
      </CardContent>
    </Card>
  );
}
