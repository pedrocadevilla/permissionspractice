import * as React from "react";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import { useState } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Modal from "@mui/material/Modal";
const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "70%",
  bgcolor: "background.paper",
  borderRadius: "10px",
  boxShadow: 24,
  p: 4,
};
export default function EditModal(props: {
  permission: Permissions | undefined;
  setSpinner: (arg0: boolean) => void;
  showModal: boolean;
  setShowModal: (arg0: boolean) => void;
  permissionTypes: PermissionsTypes[];
  getPermissions: () => void
}) {
  const handleClose = () => props.setShowModal(false);
  const [formData, setFormData] = useState({
    EmployeeName: props.permission?.employeeName,
    EmployeeLastName: props.permission?.employeeLastName,
    PermissionTypeId: props.permissionTypes.filter(
      (x) => x.description == props.permission!.permissionType
    )[0].id,
  });

  const handleInput = (e: any) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;

    setFormData((prevState) => ({
      ...prevState,
      [fieldName]: fieldValue,
    }));
  };

  const updatePermission = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent> | undefined
  ) => {
    event?.preventDefault();
    props.setSpinner(true);
    axios
      .put("http://localhost:8080/permission/" + props.permission?.id, formData)
      .then(() => {
        props.getPermissions();
        handleClose();
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        props.setSpinner(false);
      });
  };

  if (props.permission && props.permission != undefined)
    return (
      <Modal
        open={props.showModal}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Grid
          sx={style}
          component="form"
          container
          spacing={1}
          columns={16}
          noValidate
          autoComplete="off"
          justifyContent={"space-between"}
          alignItems={"center"}
        >
          <Grid item xs={4}>
            <TextField
              id="nameEdit"
              name="EmployeeName"
              label="First Name"
              variant="outlined"
              onChange={handleInput}
              value={formData.EmployeeName}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              id="lastEdit"
              name="EmployeeLastName"
              label="Last Name"
              variant="outlined"
              onChange={handleInput}
              value={formData.EmployeeLastName}
            />
          </Grid>
          <Grid item xs={4}>
            {props.permissionTypes.length > 0 && (
              <TextField
                fullWidth
                id="permissionEdit"
                name="PermissionTypeId"
                select
                label="Permission"
                defaultValue={formData.PermissionTypeId}
                onChange={handleInput}
                value={formData.PermissionTypeId}
              >
                {props.permissionTypes.map((p) => (
                  <MenuItem key={p.id} value={p.id}>
                    {p.description}
                  </MenuItem>
                ))}
              </TextField>
            )}
          </Grid>
          <Grid item xs={2}>
            <Button
              variant="outlined"
              disabled={
                formData.EmployeeName === "" ||
                formData.EmployeeLastName === "" ||
                formData.PermissionTypeId === ""
              }
              onClick={(e) => updatePermission(e)}
            >
              Update
            </Button>
          </Grid>
        </Grid>
      </Modal>
    );
}
