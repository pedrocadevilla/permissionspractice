"use client";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import axios from "axios";
import { useEffect, useState } from "react";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import EditModal from "./edit-modal";

export default function PermissionTable(props: {
  setSpinner: (arg0: boolean) => void;
  setPermissions: (arg0: Permissions[]) => void;
  permissions: Permissions[];
  permissionTypes: PermissionsTypes[];
}) {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [selectedPermission, setSelectedPermission] = useState<Permissions>();

  const deletePermission = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent> | undefined,
    id: string
  ) => {
    event?.preventDefault();
    props.setSpinner(true);
    axios
      .delete("http://localhost:8080/permission/" + id)
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        getPermissions();
      });
  };

  const editPermission = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent> | undefined,
    permission: Permissions
  ) => {
    event?.preventDefault();
    setSelectedPermission(permission);
    setShowModal(true);
  };

  const getPermissions = () => {
    axios
      .get("http://localhost:8080/permissions")
      .then((res) => {
        props.setPermissions(res.data.value);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        props.setSpinner(false);
      });
  };
  useEffect(() => {
    getPermissions();
  }, []);

  return (
    <div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} size="small">
          <TableHead>
            <TableRow>
              <TableCell align="left">First Name</TableCell>
              <TableCell align="left">Last Name</TableCell>
              <TableCell align="left">Permission</TableCell>
              <TableCell align="left">Date Assigned</TableCell>
              <TableCell align="left"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.permissions.map((p) => (
              <TableRow
                key={p.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {p.employeeName}
                </TableCell>
                <TableCell align="left">{p.employeeLastName}</TableCell>
                <TableCell align="left">{p.permissionType}</TableCell>
                <TableCell align="left">
                  {new Date(p.permissionDate).toDateString() +
                    " " +
                    new Date(p.permissionDate).toLocaleTimeString()}
                </TableCell>
                <TableCell align="left">
                  <IconButton
                    aria-label="edit"
                    color="primary"
                    onClick={(e) => editPermission(e, p)}
                  >
                    <EditIcon />
                  </IconButton>
                  <IconButton
                    aria-label="delete"
                    color="error"
                    onClick={(e) => deletePermission(e, p.id)}
                  >
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      {showModal && (
        <EditModal
          permission={selectedPermission}
          setSpinner={props.setSpinner}
          showModal={showModal}
          setShowModal={setShowModal}
          permissionTypes={props.permissionTypes}
          getPermissions={getPermissions}
        ></EditModal>
      )}
    </div>
  );
}
