import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

export default function FunctionalityCard() {
  return (
    <Card sx={{ minHeight: "100%" }}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          Functionality
        </Typography>
        <Typography variant="body2" color="text.secondary" align="justify">
          The project consist in a platform that is in charge of assign permission to users edit and delete theses assigned permissions 
        </Typography>
      </CardContent>
    </Card>
  );
}
