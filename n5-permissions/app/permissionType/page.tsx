"use client";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import axios from "axios";
import { useEffect, useState } from "react";
import LoadingSpinner from "../components/loading-spinner";
import Typography from "@mui/material/Typography";

export default function Permission() {
  const [permissions, setPermissions] = useState<PermissionsTypes[]>([]);
  const [showSpinner, setSpinner] = useState<boolean>(true);
  
  useEffect(() => {
    axios
      .get("http://localhost:8080/permissionType")
      .then((res) => {
        setPermissions(res.data.value);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setSpinner(false);
      });
  }, []);
  return showSpinner ? (
    <LoadingSpinner />
  ) : (
    <div>
      <Typography variant="h3" align="center" gutterBottom>
        Permissions Type
      </Typography>

      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 200 }} size="small">
          <TableHead>
            <TableRow>
              <TableCell align="left">Id</TableCell>
              <TableCell align="left">Description</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {permissions.map((p) => (
              <TableRow
                key={p.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {p.id}
                </TableCell>
                <TableCell align="left">{p.description}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
