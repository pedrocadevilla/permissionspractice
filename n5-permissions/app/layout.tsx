import type { Metadata } from "next";
import { Inter } from "next/font/google";
import ResponsiveAppBar from "./components/navbar";
import styles from "./page.module.css";
import Container from "@mui/material/Container";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "N5 Permission",
  description: "Created by Pedro Cadevilla",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={styles.body}>
        <ResponsiveAppBar></ResponsiveAppBar>
        <Container maxWidth="md" className={styles.container}>{children}</Container>
      </body>
    </html>
  );
}
