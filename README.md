
# Permission Project

The project consist in a dockerized app that connects sqlserver with an aspnet core 7 web api and nextjs to accomplish a very simple and straighforward permission assigment where you can see the list of permissions and create users with permissions assigned to them.


## Deployment

To deploy this project run

```bash
  docker-compose up
```
After the docker containers are deployed use the link to interact with the app

```bash
  http://localhost:3000/
```

## Authors

- [@PedroCadevilla](https://gitlab.com/pedrocadevilla)

