﻿using Application.Data;
using Domain.Errors;
using Domain.Permissions;
using Domain.PermissionTypes;
using Domain.Shared;
using MediatR;

namespace Application.Permissions.Update;

internal sealed class UpdatePermissionCommandHandler : IRequestHandler<UpdatePermissionCommand, Result<Guid>>
{
    private readonly IPermissionRepository _permissionRepository;
    private readonly IPermissionTypeRepository _permissionTypeRepository;
    private readonly IUnitOfWork _unitOfWork;

    public UpdatePermissionCommandHandler(IPermissionRepository permissionRepository, IPermissionTypeRepository permissionTypeRepository, IUnitOfWork unitOfWork)
    {
        _permissionRepository = permissionRepository;
        _permissionTypeRepository = permissionTypeRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<Result<Guid>> Handle(UpdatePermissionCommand request, CancellationToken cancellationToken)
    {
        var permission = await _permissionRepository.GetByIdAsync(request.PermissionId, cancellationToken);

        if (permission is null)
        {
            return Result.Failure<Guid>(DomainErrors.Permissions.NotFound(request.PermissionId.Value));
        }

        var permissionType = await _permissionTypeRepository.GetByIdAsync(new PermissionTypeId(permission.PermissionTypeId.Value), cancellationToken);

        if (permissionType is null)
        {
            return Result.Failure<Guid>(DomainErrors.PermissionsType.NotFound(permission.PermissionTypeId.Value));
        }

        permission.Update(
            request.EmployeeName,
            request.EmployeeLastName,
            new PermissionTypeId(request.PermissionTypeId));

        _permissionRepository.Update(permission);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return request.PermissionId.Value;
    }
}
