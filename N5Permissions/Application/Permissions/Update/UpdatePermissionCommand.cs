﻿using Domain.Permissions;
using Domain.Shared;
using MediatR;

namespace Application.Permissions.Update;

public record UpdatePermissionCommand(
    PermissionId PermissionId,
    string EmployeeName,
    string EmployeeLastName,
    Guid PermissionTypeId) : IRequest<Result<Guid>>;

public record UpdatePermissionRequest(
    string EmployeeName,
    string EmployeeLastName,
    Guid PermissionTypeId);
