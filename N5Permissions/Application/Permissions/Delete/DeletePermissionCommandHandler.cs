﻿using Application.Data;
using Domain.Errors;
using Domain.Permissions;
using Domain.Shared;
using MediatR;

namespace Application.Permissions.Delete;

internal sealed class DeletePermissionCommandHandler : IRequestHandler<DeletePermissionCommand, Result<Guid>>
{
    private readonly IPermissionRepository _productRepository;
    private readonly IUnitOfWork _unitOfWork;

    public DeletePermissionCommandHandler(IPermissionRepository productRepository, IUnitOfWork unitOfWork)
    {
        _productRepository = productRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<Result<Guid>> Handle(DeletePermissionCommand request, CancellationToken cancellationToken)
    {
        var permission = await _productRepository.GetByIdAsync(request.PermissionId, cancellationToken);

        if (permission is null)
        {
            return Result.Failure<Guid>(DomainErrors.Permissions.NotFound(request.PermissionId.Value));
        }

        _productRepository.Remove(permission);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return request.PermissionId.Value;
    }
}
