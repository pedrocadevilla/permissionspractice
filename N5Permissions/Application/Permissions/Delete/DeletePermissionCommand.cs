﻿using Domain.Permissions;
using Domain.Shared;
using MediatR;

namespace Application.Permissions.Delete;

public record DeletePermissionCommand(PermissionId PermissionId) : IRequest<Result<Guid>>;
