﻿using Domain.Shared;
using MediatR;

namespace Application.Permissions.Get;

public record GetAllPermissionQuery() : IRequest<Result<List<PermissionResponse>>>;