﻿using Application.Data;
using Domain.Errors;
using Domain.Permissions;
using Domain.Shared;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Permissions.Get;

internal sealed class GetAllPermissionQueryHandler : IRequestHandler<GetAllPermissionQuery, Result<List<PermissionResponse>>>
{
    private readonly IApplicationDbContext _context;
    private readonly IPermissionRepository _permissionRepository;

    public GetAllPermissionQueryHandler(IApplicationDbContext context, IPermissionRepository permissionRepository)
    {
        _context = context;
        _permissionRepository = permissionRepository;
    }

    public async Task<Result<List<PermissionResponse>>> Handle(GetAllPermissionQuery request, CancellationToken cancellationToken)
    {

        var permission = await _permissionRepository.GetAsync(cancellationToken);

        if (permission.Count == 0)
        {
            return Result.Failure<List<PermissionResponse>>(DomainErrors.Permissions.NoData);
        }

        var permissionsResponse = permission
            .Join(_context.PermissionTypes, p => p.PermissionTypeId, pt => pt.Id, (p, pt) => new PermissionResponse(
                p.Id.Value,
                p.EmployeeName,
                p.EmployeeLastName,
                pt.Description,
                p.PermissionDate)).ToList();

        return permissionsResponse;
    }
}
