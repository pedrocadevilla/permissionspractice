﻿using Domain.Permissions;
using Domain.Shared;
using MediatR;

namespace Application.Permissions.Get;

public record GetPermissionQuery(PermissionId PermissionId): IRequest<Result<PermissionResponse>>;

public record PermissionResponse(
    Guid Id,
    string EmployeeName,
    string EmployeeLastName,
    string PermissionType,
    DateTime PermissionDate);
