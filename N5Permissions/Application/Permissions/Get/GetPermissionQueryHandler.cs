﻿using Application.Data;
using Domain.Errors;
using Domain.Permissions;
using Domain.PermissionTypes;
using Domain.Shared;
using MediatR;

namespace Application.Permissions.Get;

internal sealed class GetPermissionQueryHandler : IRequestHandler<GetPermissionQuery,Result<PermissionResponse>>
{
    private readonly IPermissionTypeRepository _permissionTypeRepository;
    private readonly IPermissionRepository _permissionRepository;

    public GetPermissionQueryHandler(IPermissionTypeRepository permissionTypeRepository, IPermissionRepository permissionRepository)
    {
        _permissionTypeRepository = permissionTypeRepository;
        _permissionRepository = permissionRepository;
    }

    public async Task<Result<PermissionResponse>> Handle(GetPermissionQuery request, CancellationToken cancellationToken)
    {
        var permission = await _permissionRepository.GetByIdAsync(request.PermissionId, cancellationToken);

        if (permission is null)
        {
            return Result.Failure<PermissionResponse>(DomainErrors.Permissions.NotFound(request.PermissionId.Value));
        }

        var permissionType = _permissionTypeRepository.GetByIdAsync(permission.PermissionTypeId, cancellationToken);     

        return new PermissionResponse(
               permission.Id.Value,
               permission.EmployeeName,
               permission.EmployeeLastName,
               permissionType.Result!.Description,
               permission.PermissionDate);
    }
}
