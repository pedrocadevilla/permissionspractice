﻿using Application.Data;
using Domain.Errors;
using Domain.Permissions;
using Domain.PermissionTypes;
using Domain.Shared;
using MediatR;

namespace Application.Permissions.Create;

internal class CreatePermissionCommandHandler : IRequestHandler<CreatePermissionCommand, Result<Guid>>
{
    private readonly IPermissionRepository _permissionRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPermissionTypeRepository _permissionTypeRepository;

    public CreatePermissionCommandHandler(
        IPermissionRepository permissionRepository,
        IUnitOfWork unitOfWork,
        IPermissionTypeRepository permissionTypeRepository)
    {
        _permissionRepository = permissionRepository;
        _unitOfWork = unitOfWork;
        _permissionTypeRepository = permissionTypeRepository;
    }

    public async Task<Result<Guid>> Handle(CreatePermissionCommand request, CancellationToken cancellationToken)
    {
        var permissionType = await _permissionTypeRepository.GetByIdAsync(new PermissionTypeId(request.PermissionTypeId), cancellationToken);

        if (permissionType is null)
        {
            return Result.Failure<Guid>(DomainErrors.Permissions.TypeNotFound(request.PermissionTypeId));
        }

        var product = new Permission(
            new PermissionId(Guid.NewGuid()),
            request.EmployeeName,
            request.EmployeeLastName,
            new PermissionTypeId(request.PermissionTypeId));

        _permissionRepository.Add(product);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return product.Id.Value;
    }
}
