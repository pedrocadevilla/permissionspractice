﻿using Domain.Shared;
using MediatR;

namespace Application.Permissions.Create;

public record CreatePermissionCommand(
    string EmployeeName,
    string EmployeeLastName,
    Guid PermissionTypeId) : IRequest<Result<Guid>>;
