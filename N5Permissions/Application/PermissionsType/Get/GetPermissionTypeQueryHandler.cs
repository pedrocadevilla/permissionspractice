﻿using Application.Data;
using Domain.Errors;
using Domain.Shared;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.PermissionsType.Get;

internal sealed class GetPermissionTypeQueryHandler : IRequestHandler<GetPermissionTypeQuery, Result<PermissionTypeResponse>>
{
    private readonly IApplicationDbContext _context;

    public GetPermissionTypeQueryHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Result<PermissionTypeResponse>> Handle(GetPermissionTypeQuery request, CancellationToken cancellationToken)
    {
        var permissionsType = await _context
            .PermissionTypes
            .Select(pt => new PermissionTypeResponse(
                pt.Id.Value,
                pt.Description
                ))
            .FirstOrDefaultAsync(cancellationToken);

        if (permissionsType is null)
        {
            return Result.Failure<PermissionTypeResponse>(DomainErrors.PermissionsType.NotFound(request.permissionTypeId.Value));
        }

        return permissionsType;
    }
}
