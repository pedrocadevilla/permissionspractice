﻿using Domain.PermissionTypes;
using Domain.Shared;
using MediatR;

namespace Application.PermissionsType.Get;

public record GetPermissionTypeQuery(PermissionTypeId permissionTypeId) : IRequest<Result<PermissionTypeResponse>>;

public record PermissionTypeResponse(
    Guid Id,
    string Description);
