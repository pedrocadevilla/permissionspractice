﻿using Domain.Shared;
using MediatR;

namespace Application.PermissionsType.Get;

public record GetAllPermissionTypeQuery() : IRequest<Result<List<PermissionTypeResponse>>>;
