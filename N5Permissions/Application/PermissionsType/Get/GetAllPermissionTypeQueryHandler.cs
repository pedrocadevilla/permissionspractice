﻿using Application.Data;
using Domain.Errors;
using Domain.Shared;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.PermissionsType.Get;

internal sealed class GetAllPermissionTypeQueryHandler : IRequestHandler<GetAllPermissionTypeQuery, Result<List<PermissionTypeResponse>>>
{
    private readonly IApplicationDbContext _context;

    public GetAllPermissionTypeQueryHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Result<List<PermissionTypeResponse>>> Handle(GetAllPermissionTypeQuery request, CancellationToken cancellationToken)
    {
        var permissionsType = await _context
            .PermissionTypes
            .Select(pt => new PermissionTypeResponse(
                pt.Id.Value,
                pt.Description
                ))
            .ToListAsync(cancellationToken);

        if (permissionsType is null)
        {
            return Result.Failure<List<PermissionTypeResponse>>(DomainErrors.Permissions.NoData);
        }

        return permissionsType;
    }
}
