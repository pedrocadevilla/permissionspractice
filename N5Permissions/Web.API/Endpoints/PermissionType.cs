﻿using Application.PermissionsType.Get;
using Carter;
using Domain.PermissionTypes;
using MediatR;


namespace Web.API.Endpoints;

public class PermissionTypes : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {

        app.MapGet("permissionType", async (ISender sender) =>
        {
            try
            {
                var result = await sender.Send(new GetAllPermissionTypeQuery());
                if (result.IsSuccess)
                {
                    return Results.Ok(result);

                }
                else
                {
                    return Results.BadRequest(result);
                }
                
            }
            catch (Exception e)
            {
                return Results.NotFound(e);
            }
        });

        app.MapGet("permissionType/{id:guid}", async (Guid id, ISender sender) =>
        {
            try
            {
                var result = await sender.Send(new GetPermissionTypeQuery(new PermissionTypeId(id)));
                if (result.IsSuccess)
                {
                    return Results.Ok(result);

                }
                else
                {
                    return Results.BadRequest(result);
                }

            }
            catch (Exception e)
            {
                return Results.NotFound(e);
            }
        });
    }
}
