﻿using Application.Permissions.Create;
using Application.Permissions.Delete;
using Application.Permissions.Get;
using Application.Permissions.Update;
using Carter;
using Domain.Permissions;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Web.API.Endpoints;

public class Permissions : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        app.MapPost("permission", async (CreatePermissionCommand command, ISender sender) =>
        {
            await sender.Send(command);

            return Results.Ok();
        });

        app.MapGet("permissions", async (ISender sender) =>
        {
            try
            {
                return Results.Ok(await sender.Send(new GetAllPermissionQuery()));
            }
            catch (Exception e)
            {
                return Results.NotFound(e);
            }
        });

        app.MapGet("permission/{id:guid}", async (Guid id, ISender sender) =>
        {
            try
            {
                return Results.Ok(await sender.Send(new GetPermissionQuery(new PermissionId(id))));
            }
            catch (Exception e) { return Results.NotFound(e); }
        });

        app.MapPut("permission/{id:guid}", async (Guid id, [FromBody] UpdatePermissionRequest request, ISender sender) =>
        {
            var command = new UpdatePermissionCommand(
                new PermissionId(id),
                request.EmployeeName,
                request.EmployeeLastName,
                request.PermissionTypeId);
            
            await sender.Send(command);

            return Results.Ok();
        });

        app.MapDelete("permission/{id:guid}", async (Guid id, ISender sender) =>
        {
            try
            {
                await sender.Send(new DeletePermissionCommand(new PermissionId(id)));

                return Results.Ok();
            }
            catch (Exception e)
            {
                return Results.NotFound(e);
            }
        });
    }
}
