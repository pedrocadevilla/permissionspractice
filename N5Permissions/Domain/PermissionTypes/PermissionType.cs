﻿namespace Domain.PermissionTypes;

public class PermissionType
{
    public PermissionType(PermissionTypeId id, string description)
    {
        Id = id;
        Description = description;
    }

    private PermissionType()
    {
    }
    public PermissionTypeId Id { get; private set; }

    public string Description { get; private set; } = string.Empty;
}