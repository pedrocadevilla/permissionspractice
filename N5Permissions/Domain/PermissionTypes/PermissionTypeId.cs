﻿namespace Domain.PermissionTypes;

public record PermissionTypeId(Guid Value);