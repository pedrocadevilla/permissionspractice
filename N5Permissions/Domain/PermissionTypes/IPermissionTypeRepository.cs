﻿namespace Domain.PermissionTypes;

public interface IPermissionTypeRepository
{
    Task<List<PermissionType>> GetAsync(CancellationToken cancellationToken);

    Task<PermissionType?> GetByIdAsync(PermissionTypeId id, CancellationToken cancellationToken);
}
