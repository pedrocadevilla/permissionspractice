﻿using Domain.PermissionTypes;

namespace Domain.Permissions;

public class Permission
{
    public Permission(PermissionId id, string name, string lastName, PermissionTypeId permissionId)
    {
        Id = id;
        EmployeeName = name;
        EmployeeLastName = lastName;
        PermissionTypeId = permissionId;
        PermissionDate = DateTime.Now;
    }

    private Permission()
    {
    }

    public PermissionId Id { get; private set; }

    public string EmployeeName { get; private set; } = string.Empty;

    public string EmployeeLastName { get; private set; } = string.Empty;

    public PermissionTypeId PermissionTypeId { get; private set; }

    public DateTime PermissionDate { get; private set; }

    public void Update(string name, string lastName, PermissionTypeId permissionId)
    {
        EmployeeName = name;
        EmployeeLastName = lastName;
        PermissionTypeId = permissionId;
        PermissionDate = DateTime.Now;
    }
}
