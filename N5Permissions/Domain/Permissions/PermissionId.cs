﻿namespace Domain.Permissions;

public record PermissionId(Guid Value);
