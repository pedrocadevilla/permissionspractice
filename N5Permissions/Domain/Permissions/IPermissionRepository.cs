﻿using System.Threading;

namespace Domain.Permissions;

public interface IPermissionRepository
{
    Task<Permission?> GetByIdAsync(PermissionId id, CancellationToken cancellationToken);

    Task<List<Permission>> GetAsync(CancellationToken cancellationToken);

    void Add(Permission permission);

    void Update(Permission permission);

    void Remove(Permission permission);
}