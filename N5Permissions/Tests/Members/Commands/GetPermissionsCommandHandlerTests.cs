﻿using FluentAssertions;
using Moq;

using Application.Permissions.Create;
using Application.Data;
using Domain.Permissions;
using Domain.Shared;
using Domain.Errors;
using Domain.PermissionTypes;
using Application.Permissions.Get;

namespace Tests.Members.Commands;

public class GetPermissionsCommandHandlerTests
{
    private readonly Mock<IPermissionRepository> _permissionRepositoryMock;
    private readonly Mock<IPermissionTypeRepository> _permissionTypeRepositoryMock;
    private readonly Mock<IApplicationDbContext> _contextMock;

    public GetPermissionsCommandHandlerTests()
    {
        _permissionTypeRepositoryMock = new();
        _permissionRepositoryMock = new();
        _contextMock = new();
    }

    [Fact]
    public async Task Handle_Should_ReturnFailureResult_WhenNoData()
    {
        // Arrange
        var command = new GetAllPermissionQuery();
        _permissionRepositoryMock.Setup(
            x => x.GetAsync(
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(new List<Permission>());

        var handler = new GetAllPermissionQueryHandler(
            _contextMock.Object,
            _permissionRepositoryMock.Object
        );

        // Act
        Result<List<PermissionResponse>> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.Permissions.NoData);
    }

    [Fact]
    public async Task Handle_Should_ReturnFailureResult_WhenIdDoesntMatch()
    {
        // Arrange
        var permissionId = Guid.NewGuid();
        Permission? permission = null;
        var command = new GetPermissionQuery(new PermissionId(permissionId));
        _permissionRepositoryMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<PermissionId>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(permission);

        var handler = new GetPermissionQueryHandler(
            _permissionTypeRepositoryMock.Object,
            _permissionRepositoryMock.Object
        );

        // Act
        Result<PermissionResponse> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.Permissions.NotFound(permissionId));
    }
}
