﻿using FluentAssertions;
using Moq;

using Application.Permissions.Create;
using Application.Data;
using Domain.Permissions;
using Domain.Shared;
using Domain.Errors;
using Domain.PermissionTypes;

namespace Tests.Members.Commands;

public class CreatePermissionsCommandHandlerTests
{
    private readonly Mock<IPermissionRepository> _permissionRepositoryMock;
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;
    private readonly Mock<IPermissionTypeRepository> _permissionTypeMock;

    public CreatePermissionsCommandHandlerTests()
    {
        _permissionRepositoryMock = new();
        _unitOfWorkMock = new();
        _permissionTypeMock = new();
    }

    [Fact]
    public async Task Handle_Should_ReturnFailureResult_WhenPermissionTypeGuidNotFound()
    {
        // Arrange
        var typeId = Guid.NewGuid();
        PermissionType? permission = null;
        var command = new CreatePermissionCommand("first", "last", typeId);
        _permissionTypeMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<PermissionTypeId>(), 
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(permission);

        var handler = new CreatePermissionCommandHandler(
            _permissionRepositoryMock.Object,
            _unitOfWorkMock.Object,
            _permissionTypeMock.Object);

        // Act
        Result<Guid> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.Permissions.TypeNotFound(typeId));
    }

    [Fact]
    public async Task Handle_Should_ReturnSuccessResult_WhenPermissionTypeGuidFound()
    {
        // Arrange
        var typeId = Guid.NewGuid();
        var command = new CreatePermissionCommand("first", "last", typeId);
        PermissionType? permissionType = new PermissionType(new PermissionTypeId(typeId), "Description");
        _permissionTypeMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<PermissionTypeId>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(permissionType);

        var handler = new CreatePermissionCommandHandler(
            _permissionRepositoryMock.Object,
            _unitOfWorkMock.Object,
            _permissionTypeMock.Object);

        // Act
        Result<Guid> result = await handler.Handle(command, default);

        // Assert
        result.IsSuccess.Should().BeTrue();
        _permissionRepositoryMock.Verify(
            x => x.Add(It.Is<Permission>(m => m.Id == new PermissionId(result.Value))),
            Times.Once);
    }
}
