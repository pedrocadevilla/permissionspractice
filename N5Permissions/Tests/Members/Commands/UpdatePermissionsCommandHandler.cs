﻿using FluentAssertions;
using Moq;

using Application.Permissions.Create;
using Application.Data;
using Domain.Permissions;
using Domain.Shared;
using Domain.Errors;
using Domain.PermissionTypes;
using Application.Permissions.Update;

namespace Tests.Members.Commands;

public class UpdatePermissionsCommandHandlerTests
{
    private readonly Mock<IPermissionRepository> _permissionRepositoryMock;
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;
    private readonly Mock<IPermissionTypeRepository> _permissionTypeMock;

    public UpdatePermissionsCommandHandlerTests()
    {
        _permissionRepositoryMock = new();
        _unitOfWorkMock = new();
        _permissionTypeMock = new();
    }

    [Fact]
    public async Task Handle_Should_ReturnFailureResult_WhenPermissionGuidNotFound()
    {
        // Arrange
        var permissionId = Guid.NewGuid();
        var permissionTypeId = Guid.NewGuid();
        Permission? permission = null;
        var command = new UpdatePermissionCommand(new PermissionId(permissionId), "first", "last", permissionTypeId);
        _permissionRepositoryMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<PermissionId>(), 
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(permission);

        var handler = new UpdatePermissionCommandHandler(
            _permissionRepositoryMock.Object,
            _permissionTypeMock.Object,
            _unitOfWorkMock.Object);

        // Act
        Result<Guid> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.Permissions.NotFound(permissionId));
    }

    [Fact]
    public async Task Handle_Should_ReturnFailureResult_WhenPermissionTypeGuidNotFound()
    {
        // Arrange
        var permissionId = Guid.NewGuid();
        var typeId = Guid.NewGuid();
        var command = new UpdatePermissionCommand(new PermissionId(permissionId), "first2", "last2", typeId);
        PermissionType? permissionType = null;
        Permission permission = new Permission(new PermissionId(permissionId), "first", "last", new PermissionTypeId(typeId));
        _permissionRepositoryMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<PermissionId>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(permission);

        _permissionTypeMock.Setup(
            x => x.GetByIdAsync(
                It.IsAny<PermissionTypeId>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(permissionType);

        var handler = new UpdatePermissionCommandHandler(
            _permissionRepositoryMock.Object,
            _permissionTypeMock.Object,
            _unitOfWorkMock.Object);

        // Act
        Result<Guid> result = await handler.Handle(command, default);

        // Assert
        result.IsFailure.Should().BeTrue();
        result.Error.Should().Be(DomainErrors.PermissionsType.NotFound(typeId));
    }
}
