﻿using Domain.PermissionTypes;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories;

internal sealed class PermissionTypeRepository : IPermissionTypeRepository
{
    private readonly ApplicationDbContext _context;

    public PermissionTypeRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public Task<List<PermissionType>> GetAsync(CancellationToken cancellationToken)
    {
        return _context.PermissionTypes.ToListAsync(cancellationToken);
    }

    public Task<PermissionType?> GetByIdAsync(PermissionTypeId id, CancellationToken cancellationToken)
    {
        return _context.PermissionTypes.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
    }
}
