﻿using Application.Permissions.Get;
using Domain.Permissions;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories;

internal sealed class PermissionRepository : IPermissionRepository
{
    private readonly ApplicationDbContext _context;

    public PermissionRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public Task<Permission?> GetByIdAsync(PermissionId id, CancellationToken cancellationToken)
    {
        return _context.Permissions.SingleOrDefaultAsync(p => p.Id == id, cancellationToken);
    }

    public Task<List<Permission>> GetAsync(CancellationToken cancellationToken)
    {
        return _context.Permissions.ToListAsync(cancellationToken);
    }

    public void Add(Permission permission)
    {
        _context.Permissions.Add(permission);
    }

    public void Update(Permission permission)
    {
        _context.Permissions.Update(permission);
    }

    public void Remove(Permission permission)
    {
        _context.Permissions.Remove(permission);
    }
}
