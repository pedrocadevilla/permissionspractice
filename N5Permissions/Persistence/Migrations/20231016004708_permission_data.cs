﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Persistence.Migrations
{
    /// <inheritdoc />
    public partial class permission_data : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "permissions",
                columns: new[] { "id", "employee_last_name", "employee_name", "permission_date", "permission_type_id" },
                values: new object[,]
                {
                    { new Guid("0289b6aa-1713-4b24-8fb8-f3ba2827c43b"), "Haas", "Jeffery", new DateTime(2023, 10, 15, 20, 47, 8, 602, DateTimeKind.Local).AddTicks(8724), new Guid("e100f693-0b2d-4266-b186-cca57db0dcd3") },
                    { new Guid("224dc896-4553-4d65-97cd-3955d98bbbfa"), "Ho", "Hanna", new DateTime(2023, 10, 15, 20, 47, 8, 602, DateTimeKind.Local).AddTicks(8723), new Guid("e100f693-0b2d-4266-b186-cca57db0dcd3") },
                    { new Guid("275ef39e-779c-40f5-9729-4ce13ab81e0b"), "Ross", "Ean", new DateTime(2023, 10, 15, 20, 47, 8, 602, DateTimeKind.Local).AddTicks(8717), new Guid("c9c75c6b-93aa-41df-8d8d-6b0afe8e183e") },
                    { new Guid("5a61b8f7-8c8b-4da3-940e-fac24c23d14b"), "Briggs", "Aryana", new DateTime(2023, 10, 15, 20, 47, 8, 602, DateTimeKind.Local).AddTicks(8718), new Guid("c9c75c6b-93aa-41df-8d8d-6b0afe8e183e") },
                    { new Guid("82150fae-5008-4424-995b-0490c13bb87e"), "Park", "Heather", new DateTime(2023, 10, 15, 20, 47, 8, 602, DateTimeKind.Local).AddTicks(8696), new Guid("7dcc4add-9341-4e00-9327-33cf32b4b4fe") },
                    { new Guid("87f5d43d-d1bb-4ca5-9b05-a6772bc487e0"), "Downs", "Kinley", new DateTime(2023, 10, 15, 20, 47, 8, 602, DateTimeKind.Local).AddTicks(8712), new Guid("c72da965-9518-49e2-ad1c-5712ac1aaedd") },
                    { new Guid("8ded6fa2-6a58-4abe-9179-60621590b029"), "Juarez", "Raiden", new DateTime(2023, 10, 15, 20, 47, 8, 602, DateTimeKind.Local).AddTicks(8714), new Guid("c72da965-9518-49e2-ad1c-5712ac1aaedd") },
                    { new Guid("ea41d838-b37b-4699-a938-344aeb37c9f0"), "Doe", "John", new DateTime(2023, 10, 15, 20, 47, 8, 602, DateTimeKind.Local).AddTicks(8681), new Guid("264a296d-78bd-4653-9769-912144273a9a") },
                    { new Guid("ff28126f-6060-4db8-bacd-cf67a5492fc0"), "Church", "Soren", new DateTime(2023, 10, 15, 20, 47, 8, 602, DateTimeKind.Local).AddTicks(8709), new Guid("7dcc4add-9341-4e00-9327-33cf32b4b4fe") }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "permissions",
                keyColumn: "id",
                keyValue: new Guid("0289b6aa-1713-4b24-8fb8-f3ba2827c43b"));

            migrationBuilder.DeleteData(
                table: "permissions",
                keyColumn: "id",
                keyValue: new Guid("224dc896-4553-4d65-97cd-3955d98bbbfa"));

            migrationBuilder.DeleteData(
                table: "permissions",
                keyColumn: "id",
                keyValue: new Guid("275ef39e-779c-40f5-9729-4ce13ab81e0b"));

            migrationBuilder.DeleteData(
                table: "permissions",
                keyColumn: "id",
                keyValue: new Guid("5a61b8f7-8c8b-4da3-940e-fac24c23d14b"));

            migrationBuilder.DeleteData(
                table: "permissions",
                keyColumn: "id",
                keyValue: new Guid("82150fae-5008-4424-995b-0490c13bb87e"));

            migrationBuilder.DeleteData(
                table: "permissions",
                keyColumn: "id",
                keyValue: new Guid("87f5d43d-d1bb-4ca5-9b05-a6772bc487e0"));

            migrationBuilder.DeleteData(
                table: "permissions",
                keyColumn: "id",
                keyValue: new Guid("8ded6fa2-6a58-4abe-9179-60621590b029"));

            migrationBuilder.DeleteData(
                table: "permissions",
                keyColumn: "id",
                keyValue: new Guid("ea41d838-b37b-4699-a938-344aeb37c9f0"));

            migrationBuilder.DeleteData(
                table: "permissions",
                keyColumn: "id",
                keyValue: new Guid("ff28126f-6060-4db8-bacd-cf67a5492fc0"));
        }
    }
}
