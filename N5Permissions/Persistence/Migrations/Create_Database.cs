﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Persistence.Migrations
{
    /// <inheritdoc />
    public partial class Create_Database : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "permissions",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    employee_first_name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    employee_last_name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    permission_type_id = table.Column<Guid>(type: "uuid", nullable: false),
                    permission_date = table.Column<DateTime>(type: "date", nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_permissions", x => x.id);
                    table.ForeignKey(
                       name: "fk_permissiont_type_id",
                       column: x => x.permission_type_id,
                       principalTable: "permission_type",
                       principalColumn: "id",
                       onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "permissions_type",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    description = table.Column<string>(type: "text", nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_permissions_type", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_employee_first_name",
                table: "permissions",
                column: "employee_first_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_employee_last_name",
                table: "permissions",
                column: "employee_last_name");

            migrationBuilder.CreateIndex(
                name: "ix_permission_type_id",
                table: "permission_type",
                column: "id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "permissions");

            migrationBuilder.DropTable(
                name: "permissions_type");
        }
    }
}
