﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Persistence.Migrations
{
    /// <inheritdoc />
    public partial class initial_migration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "permission_types",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    description = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_permission_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "permissions",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    employee_name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    employee_last_name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    permission_type_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    permission_date = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_permissions", x => x.id);
                    table.ForeignKey(
                        name: "fk_permissions_permission_types_permission_type_temp_id",
                        column: x => x.permission_type_id,
                        principalTable: "permission_types",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "permission_types",
                columns: new[] { "id", "description" },
                values: new object[,]
                {
                    { new Guid("264a296d-78bd-4653-9769-912144273a9a"), "Client" },
                    { new Guid("7dcc4add-9341-4e00-9327-33cf32b4b4fe"), "Reader" },
                    { new Guid("c72da965-9518-49e2-ad1c-5712ac1aaedd"), "Employee" },
                    { new Guid("c9c75c6b-93aa-41df-8d8d-6b0afe8e183e"), "Super" },
                    { new Guid("e100f693-0b2d-4266-b186-cca57db0dcd3"), "Admin" }
                });

            migrationBuilder.CreateIndex(
                name: "ix_permissions_permission_type_id",
                table: "permissions",
                column: "permission_type_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "permissions");

            migrationBuilder.DropTable(
                name: "permission_types");
        }
    }
}
