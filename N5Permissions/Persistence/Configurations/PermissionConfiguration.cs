﻿using Domain.Permissions;
using Domain.PermissionTypes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations;

internal class PermissionConfiguration : IEntityTypeConfiguration<Permission>
{
    public void Configure(EntityTypeBuilder<Permission> builder)
    {
        builder.HasKey(p => p.Id);

        builder.Property(p => p.Id).HasConversion(
            permissionId => permissionId.Value,
            value => new PermissionId(value));

        builder.HasOne<PermissionType>()
            .WithMany()
            .HasForeignKey(p => p.PermissionTypeId)
            .IsRequired();

        builder.HasData(
            new Permission(new PermissionId(Guid.NewGuid()), "John", "Doe", new PermissionTypeId(new Guid("264a296d-78bd-4653-9769-912144273a9a"))),
            new Permission(new PermissionId(Guid.NewGuid()), "Heather", "Park", new PermissionTypeId(new Guid("7dcc4add-9341-4e00-9327-33cf32b4b4fe"))),
            new Permission(new PermissionId(Guid.NewGuid()), "Soren", "Church", new PermissionTypeId(new Guid("7dcc4add-9341-4e00-9327-33cf32b4b4fe"))),
            new Permission(new PermissionId(Guid.NewGuid()), "Kinley", "Downs", new PermissionTypeId(new Guid("c72da965-9518-49e2-ad1c-5712ac1aaedd"))),
            new Permission(new PermissionId(Guid.NewGuid()), "Raiden", "Juarez", new PermissionTypeId(new Guid("c72da965-9518-49e2-ad1c-5712ac1aaedd"))),
            new Permission(new PermissionId(Guid.NewGuid()), "Ean", "Ross", new PermissionTypeId(new Guid("c9c75c6b-93aa-41df-8d8d-6b0afe8e183e"))),
            new Permission(new PermissionId(Guid.NewGuid()), "Aryana", "Briggs", new PermissionTypeId(new Guid("c9c75c6b-93aa-41df-8d8d-6b0afe8e183e"))),
            new Permission(new PermissionId(Guid.NewGuid()), "Hanna", "Ho", new PermissionTypeId(new Guid("e100f693-0b2d-4266-b186-cca57db0dcd3"))),
            new Permission(new PermissionId(Guid.NewGuid()), "Jeffery", "Haas", new PermissionTypeId(new Guid("e100f693-0b2d-4266-b186-cca57db0dcd3")))
        );
    }
}
