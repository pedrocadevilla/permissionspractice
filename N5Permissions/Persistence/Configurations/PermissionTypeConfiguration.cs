﻿using Domain.PermissionTypes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations;

internal class PermissionTypeConfiguration : IEntityTypeConfiguration<PermissionType>
{
    public void Configure(EntityTypeBuilder<PermissionType> builder)
    {
        builder.HasKey(pt => pt.Id);

        builder.Property(pt => pt.Id).HasConversion(
            permissionTypeId => permissionTypeId.Value,
            value => new PermissionTypeId(value));

        builder.Property(pt => pt.Description).HasMaxLength(100);

        builder.HasData(
            new PermissionType(new PermissionTypeId(Guid.NewGuid()), "Super"),
            new PermissionType(new PermissionTypeId(Guid.NewGuid()), "Admin"),
            new PermissionType(new PermissionTypeId(Guid.NewGuid()), "Employee"),
            new PermissionType(new PermissionTypeId(Guid.NewGuid()), "Client"),
            new PermissionType(new PermissionTypeId(Guid.NewGuid()), "Reader")

        );
    }
}
